use strict;
use warnings;

# this test was generated with Dist::Zilla::Plugin::Test::EOL 0.19

use Test::More 0.88;
use Test::EOL;

my @files = (
    'lib/Test/WWW/Mechanize/Mojo.pm',
    't/00-compile.t',
    't/auth-test.t',
    't/cookies.t',
    't/form_submit.t',
    't/lib/MyMojjy.pm',
    't/redirect.t',
    't/simple.t',
    't/useragent.t',
    't/white_label.t'
);

eol_unix_ok($_, { trailing_whitespace => 1 }) foreach @files;
done_testing;
