use strict;
use warnings;

# this test was generated with Dist::Zilla::Plugin::Test::NoTabs 0.15

use Test::More 0.88;
use Test::NoTabs;

my @files = (
    'lib/Test/WWW/Mechanize/Mojo.pm',
    't/00-compile.t',
    't/auth-test.t',
    't/cookies.t',
    't/form_submit.t',
    't/lib/MyMojjy.pm',
    't/redirect.t',
    't/simple.t',
    't/useragent.t',
    't/white_label.t'
);

notabs_ok($_) foreach @files;
done_testing;
